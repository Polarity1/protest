<?php include_once('server.php');
        include_once('login_php.php');
 ?>
<!DOCTYPE HTML>
<html>
<head>
<title>PHP en MYSkool registratie systeem</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="header">
        <h2>Login</h2>
    </div>

    <form method="post" action="login.php">

            <?php include("errors.php"); ?>

        <div class="input-group">
            <label>Username</label>
            <input type="text" name="email" placeholder="Email@example.com" autofocus>
        </div>

        <div class="input-group">
            <label>Password</label>
            <input type="password" name="password" placeholder="password">
        </div>

        <div class="input-group">
            <button type="submit" name="login" class="btn">Login</button>
        </div>
        <p>Not a member? <a href="register.php">Register</a>
    </form>
</body>
</html>