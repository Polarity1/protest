<?php

if (isset($_POST['register'])) {
    $username = mysqli_real_escape_string($db, $_POST['username'] );
    $email = mysqli_real_escape_string($db, $_POST['email'] );
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1'] );
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2'] );

    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($email)) {
        array_push($errors, "E-mail is required");
    }
    if (empty($password_1)) {
        array_push($errors, "Password is required");
    }

    if ($password_1 != $password_2) {
        array_push($errors, "The two passwords do not match");
    }

    if (count($errors) == 0) {
        $query = "SELECT DISTINCT * FROM users WHERE email ='$email'";
        if (mysqli_num_rows(mysqli_query($db, $query)) == 0) {
            $password = password_hash($password_1, PASSWORD_DEFAULT);
            $sql = "INSERT INTO users (username, email, password) VALUES ('$username', '$email', '$password')";
            mysqli_query($db, $sql);
            date_default_timezone_set("Europe/Amsterdam");
            $_SESSION['username'] = $username;
            $_SESSION['email'] = $email;
            $_SESSION['created'] = date('Y-m-d H:i:s');
            $_SESSION['userclass'] = "guest";
            $_SESSION['success'] = "You are now logged in.";
            mail($email, "Registration", "You have registered at my site!");   
            unset($email, $password, $username);
            header('location: index.php');
        } else {
            array_push($errors, "E-mail is already in use");
        }
    } 
}
?>