<?php 
if (isset($_POST['login'])) {
    $email = mysqli_real_escape_string($db, $_POST['email'] );
    $password = mysqli_real_escape_string($db, $_POST['password'] );

    if (empty($email)) {
        array_push($errors, "Email is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

    if (count($errors) == 0) {
        $query = "SELECT DISTINCT * FROM users WHERE email='$email'";
        $result = mysqli_query($db, $query);
        if (mysqli_num_rows($result) == 1) {
            $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
            if (password_verify($password ,$row['password'])) {
                $_SESSION['username'] = $row['username'];
                $_SESSION['email'] = $row['email'];
                $_SESSION['id'] = $row['id'];
                $_SESSION['created'] = $row['created'];
                $_SESSION['userclass'] = $row['userclass'];
                $_SESSION['userrating'] = $row['userrating'];
                date_default_timezone_set("Europe/Amsterdam");
                $timestamp = date("Y-m-d H:i:s");
                $update_last_login = "UPDATE users SET last_login='$timestamp' WHERE id=$_SESSION[id]";
                mysqli_query($db, $update_last_login);
                header('location: index.php');          
            } else {
                array_push($errors, "Wrong password");
            }
        } else {
            array_push($errors, "Wrong email/password combination");
        }
    }
}

?>