<?php 
include_once("server.php");
include("navbar.php");
include('update_profile.php');
$id = $_SESSION['id'];
$sql = "SELECT * FROM user_order WHERE user_id='$id'";
 
$result = mysqli_query($db, $sql);
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Profile</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="header">
            <h2>Profile</h2>
        </div>

        <form method="post" action="profile.php">
            
            <?php include("errors.php"); ?>

            <div class="input-group">
                <label>Username</label>
                <input type="text" name="username" value="<?php echo $_SESSION['username']; ?>">
            </div>

            <div class="input-group">
                <label>E-mail</label>
                <input type="email" name="email" value="<?php echo $_SESSION['email']; ?>">
            </div>

            <div class="input-group">
                <label>Password</label>
                <input type="password" name="password_1">
            </div>

            <div class="input-group">
                <label>Confirm password</label>
                <input type="password" name="password_2">
            </div>

            <div class="input-group">
                <label>Created</label>
                <input type="email" name="email" value="<?php echo $_SESSION['created']; ?>" disabled>
            </div>

            <div class="input-group">
                <label>Orders</label>
                    <?php 
                    if (mysqli_num_rows($result) > 0) {
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "<a href=order.php?oid=" . $row['order_id'] . ">" . $row['order_id'] . "</a><br>";
                        }
                    }
                    ?>
            </div>

            <div class="input-group">
                <button type="submit" name="update" class="btn">Update</button>
                <button type='submit' name='delete1' class='btn'><a href='delete_user_from_db.php?id=<?php echo $_SESSION['id'] ?>' onclick='javascript:return confirm("Are you sure you want to delete your account?")'>Delete Account</a></button>
                <button type="submit" name="back" class="btn"><a href="index.php">Back</a></button>
            </div>
            <?php include('update_profile.php'); ?>
        </form>
      
    </body>
</html>