<?php
include('server.php');
include('navbar.php');

$sql = "SELECT * FROM producten";
$result = mysqli_query($db, $sql);
$errors = array();
?>

<html>
    <head>
        <title>Product DB management</title>
        <link rel="stylesheet" href="style.css">
    </head>

<body>

    <div class="header">
        <h2>Product Management</h2>
    </div>
    <div class="content">
        <table>
            <tr>
                <th class='id'>ID:</th>
                <th>Product:</th>
                <th>Price: (&euro;)</th>
                <th>Description:</th>
                <th class="id">Stock:</th>
            <tr>
                <tr></tr>
<?php

    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr><td id='1'>" . $row['id'] . "</td><td>" . $row['product'] . "</td><td>" . str_replace(".",",",$row['prijs']) . "</td>
            <td>" . $row['omschrijving'] . "</td>";
            if ($row['stock'] <= 15) {
                array_push($errors, $row['product']);
                echo "<td style='color: red'> $row[stock]";
            } else {
                echo "<td style='color: green'> $row[stock]";
            }
            echo "</td><td><a href='#' onclick='delete_item();'><img src='delete.png'></a></td></tr>";
        } 
    } else {
        echo "<br>No Products<br><br>";
    }
?>

        </table>
        <?php 
        echo "<div class='alert'>";
        if (count($errors) > 2) {
            echo "Alert!&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Hover over to see<br>Some products (" . count($errors) . ") are low on stock!<br>Consider re-ordering them!";
        } else {
            echo "Alert!<br>One product is low on stock!<br>Consider re-ordering it!";
        }    
        echo "<div class='lowonstock'><br>";    
        foreach ($errors as $error) {
            echo $error . "<br>";
        }
        echo "</div></div>";
        ?>
    </div>
<script type="text/javascript">

    function delete_item() {
        
        var id = event.target.parentNode.parentNode.parentNode.firstChild.innerHTML;
        event.target.parentNode.parentNode.parentNode.remove();
        var url = "delete_item_from_db.php?id=" + id;
        var myWindow = window.open(url, "Deleting...", "width=10,height=10");
        setTimeout(function() {
           myWindow.close();  
        }, 100);
    }
    
</script>
</body>
</html>