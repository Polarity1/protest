<?php
if (isset($_POST['add'])) {
      $file_name = $_FILES['image']['name'];
      $file_size =$_FILES['image']['size'];
      $file_tmp =$_FILES['image']['tmp_name'];
      $file_type=$_FILES['image']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
      $expensions= array("jpeg","jpg","png");
      $product = mysqli_real_escape_string($db, $_POST['product'] );
      $price = mysqli_real_escape_string($db, $_POST['price'] );
      $description = mysqli_real_escape_string($db, $_POST['description'] );
      
      if (empty($file_name)) {
        $file_name = "stock.jpg";
        $file_ext=strtolower(end(explode('.',$file_name)));
      }

      if (in_array($file_ext,$expensions)=== false){
        array_push($errors, "Extension not allowed, please choose a JPEG or PNG file.");
      }
    
    if (empty($product)) {
        array_push($errors, "Product name is required");
    }
    if (empty($price)) {
        array_push($errors, "Price is required");
    }
    if (empty($description)) {
        array_push($errors, "Description is required");
    }

    if (count($errors) == 0) {
    move_uploaded_file($file_tmp,"images/".$file_name);
    $query = "INSERT INTO producten (product, prijs, omschrijving, image) VALUES ('$product', '$price', '$description', '$file_name')";
    mysqli_query($db, $query);
    header("location: index.php");
        
    }
    unset($product, $price, $description, $file_name);
}
?>