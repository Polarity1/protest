<?php 
include('server.php');
include("navbar.php");
include('query.php');
//include('shopcart.php');

    if (empty($_SESSION['username'])) {
        header('location: login.php');
    }

?>
<!DOCTYPE HTML>
<html>
<head>
<title>PHP en MYSkool registratie systeem</title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="style1.css">
</head>
<body>
    <div class="header">
        <h2>Home page</h2>
    </div>
 <div class="content">

    <?php if (isset($_SESSION['username'])): ?>
        <p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
        <p>Today is: <strong><?php echo date('l \t\h\e jS \o\f F\, Y'); ?></strong></p>
        <p>You have created your account:<strong> 
        <?php 
        date_default_timezone_set("Europe/Amsterdam");
        $time1 = strtotime($_SESSION['created']);
        $time2 = strtotime(date('Y-m-d H:i:s'));
        $secs = $time2 - $time1;
        $days = $secs / 86400;
        echo intval($days).  "d ". intval((($days - intval($days)) * 24)) . "h " . intval((((($days - intval($days)) * 24) - intval((($days - intval($days)) * 24))) *60)) ."m";
        ?> </strong> ago</p>
        
<br>
<form action="index.php" method="post">
    <div class="search-group">
        <label>Prijs</label>&ensp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<label>Product</label><br>
        <input type="text" name="price" value="<?php echo str_replace(".",",",$prijs) ?>">
        <input type="text" name="prodquer" value="<?php echo $prodq ?>">
    </div>
    <button type="submit" class="btn" name="query">Zoeken...</button>
    <select name="order">
        <option value="prijs asc">price asc</option>
        <option value="prijs desc">price desc</option>
</select>
</form>
    <div class="r_cont">
        <?php include('result.php'); ?>
</div>
  
    <?php endif ?>
 
 </div>
<script type="text/javascript">
    var basket;
    if (!sessionStorage.getItem("products")) {
        basket = [];
    } else {
        basket = JSON.parse(sessionStorage.getItem("products"));
    }
    
    function add() {
        var amount = event.target.parentNode.parentNode.firstChild.value;
        var prod = event.target.parentNode.parentNode.parentNode.parentNode.childNodes[2].firstChild.innerHTML;
        var id = event.target.parentNode.parentNode.parentNode.parentNode.childNodes[1].innerHTML;
        id = id.substr(3);
        var prijs = event.target.parentNode.parentNode.parentNode.innerHTML;
        prijs = prijs.substring(0, prijs.indexOf("<"));
        prijs = prijs.substring((prijs.indexOf("€") +1));
        let product = {};
        product.id = id;
        product.product = prod;
        product.prijs = prijs;
        product.amount = amount;
        basket.push(product);
        sessionStorage.setItem("products", JSON.stringify(basket));
        update_basket();
    }

    function update_basket() {
        document.getElementById("amount").innerHTML = basket.length;

    }
    update_basket();

    var clicks = document.getElementsByClassName("inputbox");
    for (x of clicks) {
        x.addEventListener("click", function() {
            let id = event.target.parentNode.parentNode.childNodes[1].innerHTML;
            id = id.substr(3);
            let url = "product.php?prodid=" + id;
            window.open(url, "_self");
        });

    }

</script>
</body>
</html>