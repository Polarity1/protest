<?php
include('server.php');
include('navbar.php');
?>

<html>
    <head>
        <title>Shopping Basket</title>
        <link rel="stylesheet" href="style.css">
    </head>
<body>
<div class="header">
    <h2>Basket</h2>
</div>
<div class="content">
    <table id="table">
        <tr>
            <th class='id'>Product:</th>
            <th>Price: (&euro;)</th>
            <th>Amount:</th>
            <th>Total: (&euro;)</th>
        <tr>
        <tr>
        </tr>
    </table>
    <div id='buttons'>
    </div>
    <div id="buttons2">
    </div>
</div>

<script>
    var total = 0;

if (!sessionStorage.getItem("products")) {
    products = [];
} else {
    products = JSON.parse(sessionStorage.getItem("products"));
}
if (products.length != 0) {
for (let i = 0; i < products.length; i++) {
    var price = products[i].prijs;
    var newTr = document.createElement("tr");
    var newTd1 = document.createElement("td"); 
    var newContent1 = document.createTextNode(products[i].product); 
    newTd1.classList.add("eerste");
    var newTd2 = document.createElement("td"); 
    var newContent2 = document.createTextNode(price); 
    var newTd3 = document.createElement("td"); 
    var newContent3 = document.createTextNode(products[i].amount); 
    var newTd4 = document.createElement("td"); 
    price = parseFloat(price.replace(",", "."));
    var amount = parseFloat(products[i].amount);
    var prodtotal = price * amount;
    total += prodtotal;
    prodtotal = prodtotal.toFixed(2);
    prodtotal = prodtotal.replace(".",",");
    var newContent4 = document.createTextNode(prodtotal); 
        
    newTd1.append(newContent1); 
    newTd2.append(newContent2);
    newTd3.append(newContent3);
    newTd4.append(newContent4);

    newTr.append(newTd1);
    newTr.append(newTd2);
    newTr.append(newTd3);
    newTr.append(newTd4);

    var currentDiv = document.getElementById("table"); 
    currentDiv.append(newTr); 
    
}
total = total.toFixed(2);
total = total.replace(".",",");
 var newTr = document.createElement("tr");
 var newTd1 = document.createElement("td");
 var newTd2 = document.createElement("td");
 var newTd3 = document.createElement("td");
 var newTd4 = document.createElement("td");
 var newTd5 = document.createElement("td");
 var newInp = document.createTextNode(total);
 newTr.append(newTd1);
 newTr.append(newTd2);
 newTr.append(newTd3);
 newTr.append(newTd4);
 newTr.append(newTd5);
 newTd1.append(newInp);
 newTr.append(newTd1);
 var currentDiv = document.getElementById("table"); 
 currentDiv.append(newTr); 
 document.getElementById("amount").innerHTML = products.length;

 function remove_all() {
    sessionStorage.removeItem("products");
    window.location.reload(true);
 }

    //button 1
    var newButton = document.createElement("button");
    var newContent = document.createTextNode("Remove All"); 
    newButton.append(newContent);
    newButton.classList.add("btn"); 
    newButton.setAttribute("onclick", "remove_all();");
    document.getElementById("buttons2").append(newButton);

    //button 2
    var newButton = document.createElement("button");
    var newContent = document.createTextNode("Order now!"); 
    newButton.append(newContent);
    newButton.classList.add("btn"); 
    newButton.setAttribute("onclick", "order_now();");
    newButton.setAttribute("type", "submit");
    newButton.setAttribute("name", "order");
    document.getElementById("buttons").append(newButton);

}

function order_now() {
    var ids = "";
    var amount = "";
    for (x in products) {
        if (x == products.length -1) {
            ids += products[x].id;
            amount += products[x].amount;
        } else {
            ids += products[x].id + ",";
            amount += products[x].amount + ",";
        }
    }
    var url = "payment.php?id=" + ids + "&amount=" + amount;
    window.open(url, "_self");
}
</script>    
</body>
</html>