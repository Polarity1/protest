 <?php
    include('server.php');
    $id = $_GET['id'];
    $db = $_SESSION['db'];
    $sql = "DELETE FROM users WHERE id=$id";

if ($_SESSION['userclass'] == "admin") {   
 
    mysqli_query($db, $sql);
 
} else if ($_SESSION['id'] == $id) {

    mysqli_query($db, $sql);
    session_destroy();
    header('location: login.php');

} else {

    echo "You don't have the privileges to delete users from the database";

}

?>