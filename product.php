<?php
    $valuta = "&euro;";
    include('navbar.php');
    include('server.php');

    if (isset($_GET['prodid'])) {
        $productid = $_GET['prodid'];
    } else {
        $productid = 50;
    }
    
    $sql = "SELECT * FROM producten WHERE id='$productid'";
    $result = mysqli_query($db, $sql);
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $product = $row['product'];
            $prijs = $row['prijs'];
            $omschrijving = $row['omschrijving'];
            $stock = $row['stock'];
            $image = $row['image'];
        }
    } else { $omschrijving = "Ongeldig ID"; }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Product <?= $product ?></title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="header">
        <h2><?= $product ?></h2>
    </div>
 <div class="prod_content">
    <div class="prod_inner_cont">
    <div class="prod_image">
        <img src='images/<?= $image ?>' width="225px">
    </div>
    <div class='product_ppage'>
    Product: <?= $product ?>
    </div>
    <div class='prod'>
      
        <?= $omschrijving ?>
    </div>
    <div class="pprijs">

        <?= $valuta ?><?= $prijs ?>
        <?php
        if ($stock == 1) {
            $color = "red";
            $instock = "{$stock} item in stock!"; 
        } else if ($stock <= 15) {
            $color = "red";
            $instock = "{$stock}  items in stock!";
        } else if (($stock > 15) && ($stock <= 40)) {
            $color = "orange";
            $instock = "{$stock}  items in stock!";
        } else {
            $color ="green";
            $instock = "{$stock}  items in stock!";
        }
        
        ?>
         <div class='stock_cont'>
             <br>Items in stock: <?= $stock ?>
       <div class="stock" title="<?= $instock ?>" style="background-color:<?= $color ?>; width:25px; height:25px; margin-left:49%;"></div>
    </div>
    </div>
    <div class="button">
        <a href="index.php" class="btn">Back</a>
    </div>
    </div>
</div>
</body>
</html>
    