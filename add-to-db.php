<?php include_once("server.php");
    include('product_add.php');
    include('navbar.php');
?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>Add items to Product DB</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="header">
            <h2>Add items to Database</h2>
        </div>

        <form method="post" action="add-to-db.php" enctype="multipart/form-data">
            <?php include('errors.php'); ?>
            <div class="input-group">
                <label>Product</label>
                <input type="text" name="product">
            </div>

            <div class="input-group">
                <label>Prijs</label>
                <input type="text" name="price">
            </div>

            <div class="input-group">
                <label>Omschrijving</label>
                <input type="text" name="description">
            </div>


            <div>
                <label>Upload image</label><br><br>
                <input type="file" name="image">
            </div>
            <div class="input-group">
                <button type="submit" name="add" class="btn">Add to DB</button>
            </div>
            <p><a href="index.php" class="btn">Home</a>
        </form>
    </body>
</html>