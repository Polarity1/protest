<?php
include('server.php');
include('navbar.php');

$sql = "SELECT * FROM users ORDER BY userclass asc";
$result = mysqli_query($db, $sql);

?>

<html>
    <head>
        <title>User management</title>
        <link rel="stylesheet" href="style.css">
    </head>

<body>

    <div class="header">
        <h2>User Management</h2>
    </div>
    <div class="content">
        <table>
            <tr>
                <th class='id'>User ID:</th>
                <th>Username:</th>
                <th>E-Mail:</th>
                <th>Role:</th>
                <th class='id'>Rating:</th>
            <tr>
                <tr></tr>
<?php
$selectedAdmin = '';
$selectedUser = '';
$selectedGuest = '';

    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['userclass'] == "admin") {
                $selectedAdmin = 'selected';
            } else if ($row['userclass'] == "user") {
                $selectedUser = 'selected';
            } else {
                $selectedGuest = 'selected';
            }
            echo "<tr><td id='1'>" . $row['id'] . "</td><td>" . $row['username'] . "</td><td>" . $row['email'] . "</td><td>
            <select id='ucsel' onchange='update();'>
            <option value='guest'" . $selectedGuest . ">guest   
            <option value='user'" . $selectedUser . ">user
            <option value='admin'" . $selectedAdmin . ">admin
            </select></td><td class='rating'><input type='number' min='0' max='100' value='" . $row['userrating'] . "'disabled></td><td>";
            if ($_SESSION['id'] != $row['id']) {
                echo "<a href='#' onclick='delete_id(); return false;'><img src='delete.png'></a></td></tr>";
            }
            unset($selectedAdmin, $selectedUser, $selectedGuest);
        } 
    } else {
        echo "<br>No users<br><br>";
    }
?>

        </table>
    </div>
<script type="text/javascript">
    function update() {
        var uc = event.target.value;
        var id = event.target.parentNode.parentNode.firstChild.innerHTML;
        var url = "update_user_db.php?id=" + id + "&uc=" + uc;
        var myWindow = window.open(url, "Updating", "width=10,height=10");
        setTimeout(function() {
            myWindow.close();  
        }, 100);
    }
      
    function delete_id() {

        var id = event.target.parentNode.parentNode.parentNode.firstChild.innerHTML;
        var url = "delete_user_from_db.php?id=" + id;
        var myWindow = window.open(url, "Deleting...", "width=10,height=10");
        event.target.parentNode.parentNode.parentNode.remove(); 
        setTimeout(function() {
            myWindow.close();  
        }, 100);
    }
</script>  
</body>
</html>